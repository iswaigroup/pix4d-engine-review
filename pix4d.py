
import sys
from datetime import datetime
import os 
from enum import Enum


try:
	from pix4dengine import create_project, login_seat, logout_seat
	from pix4dengine.pipeline import Pipeline
	from pix4dengine.constants.processing import ProcessingStep
	from pix4dengine.options import ExportOption
	from pix4dengine.exports import get_report
	from pix4dengine.options import AlgoOption
	from pix4dengine.utils.errors import FailedValidation
	from pix4dengine.algo import calib
	from pix4dengine.utils.report import Quality
	from pix4dengine.pipeline import apply_template
	from pix4dengine.pipeline.templates import (
		RapidModel3D,  # Quick generation of a 3D model
		MeshLowRes,  # Reduce the mesh resolution
		Model3D,
		MeshNormalRes,
		MeshHighRes,
		Default,
		ThermalCamera,
		TemplateOptions,
	)
	import pix4dengine.pipeline.templates
	from pix4dengine.algo import dense, mesh

except Exception as e:
	pass



try:
	class Model3DISW(Enum):
	# 	"""This template only applies to CALIB algo."""
		CALIB = TemplateOptions(
			base=Model3D.CALIB,
			# Modify the matching distance wrt RapidModel3D.CALIB
			options={
				calib.AlgoOption.IMAGE_SCALE: "1",
				#calib.AlgoOption.CameraCalibration.MATCH_STRATEGY : "AutoOblique",
				calib.AlgoOption.MATCH_GEOMETRICALLY_VERIFIED : True,
				calib.AlgoOption.KEYPT_NUMBER: 10000,
				calib.AlgoOption.CALIBRATION_METHOD: "Standard",
				calib.AlgoOption.CALIBRATION_EXT_PARAM_OPT: 'All',
				calib.AlgoOption.CALIBRATION_INT_PARAM_OPT: 'All',
				calib.AlgoOption.REMATCH: False,
				calib.ExportOption.UNDISTORTED_IMAGES: False,
				calib.AlgoOption.MATCH_GEOMETRICALLY_VERIFIED: True,
			},
		)

		DENSE = TemplateOptions(
			base=Model3D.DENSE,
			# Modify the matching distance wrt Model3D.CALIB
			options={
					dense.ExportOption.PCL_LAS: True,
					dense.ExportOption.PCL_XYZ: True,
					dense.AlgoOption.PCL_DENSITY: "Optimal", 
					dense.AlgoOption.PCL_IMAGE_MULTISCALE: True,
					dense.AlgoOption.PCL_MIN_NO_MATCHES : 4,
					dense.AlgoOption.PCL_XYZ_DELIMITER :  'Space',
					dense.AlgoOption.PCL_MERGE_TILES: True,
					dense.AlgoOption.PCL_WINDOWS_SIZE: 9,
					dense.AlgoOption.PCL_USE_PROCESSING_AREA: True, 
					dense.AlgoOption.PCL_USE_ANNOTATIONS: True,
					mesh.AlgoOption.TEXTURE_SIZE: 8192, 
					mesh.AlgoOption.DECIMATION_CRITERIA: 'Quantitative',
					mesh.AlgoOption.MAX_TRIANGLES: 500000,
					mesh.AlgoOption.DECIMATION_STRATEGY: "Sensitive", 
					mesh.AlgoOption.TEXTURE_COLOR_BALANCING: True,
					mesh.AlgoOption.MAX_OCTREE_DEPTH: 12,
					mesh.AlgoOption.SAMPLE_DENSITY_DIVIDER: 1,
					mesh.ExportOption.PLY: True,
					mesh.ExportOption.FBX: True,
					mesh.ExportOption.OBJ: True,
			}
		)

except Exception as e:
	pass





def pix4d_engine(img_dir, prj_name='test', prj_dir='tmp', template=Model3D, username=None, password=None):

	if username is None:
		username = os.environ['PIX4D_USER']
		password = os.environ['PIX4D_PASSWORD']


	try:

		# Acquire the authorization to use Pix4Dengine from the Pix4D licensing system
		login_seat(username, password)
		# Acquire the authorization to use Pix4Dengine from the Pix4D licensing system
		# using a specific license. This is useful if you have more than one license.

		# Create a project
		project = create_project(prj_name,
								image_dirs=img_dir, work_dir=prj_dir)

		# Calibrate the cameras in the project

		pipeline = Pipeline(project, algos=["CALIB", "DENSE"])
		apply_template(pipeline, template)		


		pipeline.get_task("CALIB").set_callbacks(
			on_start=lambda: logging.log(task_id=sub_task_id, task_conf=task_conf, status='RUNNING', log='Starting Camera calibration'),
			on_success=lambda: logging.log(task_id=sub_task_id, task_conf=task_conf, status='RUNNING', log='Camera calibration succesful'),
			on_error= lambda: logging.log(task_id=sub_task_id, task_conf=task_conf, status='FAILURE', log='Camera calibration failed'),
		)


		pipeline.get_task("DENSE").update_config(
			{
				dense.ExportOption.PCL_LAS: True,
				dense.ExportOption.PCL_XYZ: True,
				# dense.AlgoOption.PCL_DENSITY: "High", 
				mesh.ExportOption.PLY: True,
				mesh.ExportOption.FBX: True,
				mesh.ExportOption.OBJ: True,
			}                                        
		)

		pipeline.get_task("DENSE").set_callbacks(
			on_start=lambda: print('Starting point cloud densification'),
			on_success=lambda: print('Point cloud densification successful!'),
			on_error= lambda: print('Point cloud densification failed!'),
		)
		pipeline.run()

	except Exception as x:
		print(x)


	try:
		quality_report = get_report(project)

		print(quality_report.calibration_quality_status())
		print(quality_report.absolute_geolocation_rms())
		print(quality_report.image_dataset_info())

		print(
			"Calibration quality status = %s." % quality_report.calibration_quality_status().dataset.value
		)

		print(
			"Number of 3D GCPs used to georeference the project = %d." % quality_report.number_of_3d_gcps()
		)
		print("Camera optimization relative difference = %d." % quality_report.camera_opt_rel_diff())

	except FailedValidation as stop:
		print(stop)

