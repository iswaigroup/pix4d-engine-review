from pix4d import pix4d_engine, Model3DISW

import pix4dengine
import pix4dengine.pipeline.templates

import argparse
import os


parser = argparse.ArgumentParser(description='pix4d engine testing')

parser.add_argument('--img_dir', type=str, default='/data/datasets/',
					   help='path to images')

parser.add_argument('--prj_dir', type=str, default='/data/tmp/',
					   help='path to pix4d project folder')

parser.add_argument('--name', type=str, default='test',
					   help='pix4d project name')

parser.add_argument('--template', type=str, default='Model3DISW',
					   help='pix4d template name')

parser.add_argument('--pix4d_user', type=str,
					   help='pix4d username')

parser.add_argument('--pix4d_password', type=str,
					   help='pix4d username')




										   
args = parser.parse_args()





if __name__ == '__main__':

    img_dir = args.img_dir
    prj_dir = args.prj_dir
    prj_name = args.name
    template = args.template
    pix4d_username = args.pix4d_user
    pix4d_password = args.pix4d_password


    try:
        if hasattr(pix4dengine.pipeline.templates, template):
            template = getattr(pix4dengine.pipeline.templates, template)
        else:
            template = globals()[template]
    except Exception as e:
        template = pix4dengine.pipeline.templates.Model3D
        print(e)
        print('tempate does not exist - setting Model3D as base template')


    try:
        os.makedirs(prj_dir)
    except Exception as e:
        pass

    pix4d_engine(img_dir, prj_name, prj_dir, template, pix4d_username, pix4d_password)

