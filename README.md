




## build docker image using the following command
```
docker build -t pix4dengine pix4d-engine-review/
```

## run continer with test data
```
docker run -ti --rm -v "/home/ai/Desktop/test_data/1662/:/data/" pix4dengine "python3 main.py --img_dir /data/RGB --prj_dir /data/tmp/ --template Model3DISW --pix4d_user USER --pix4d_password PASSWORD"

```

you can add the following flags if required: 
```
--img_dir image folder (note that it should be under the mounted directory "/PATH/TO/DATASET" and with with respect to mounted location /data/)

--template Specify any of pix4d templates (Model3D, Default, RapidModel3D, etc..). Default: Our custom made tempate by default.

--name pix4d project name

--prj_dir location of pix4d project (note that it should be under the mounted directory "/PATH/TO/DATASET" and with with respect to mounted location /data/)


--pix4d_user a valid user account with pix4d engine

--pix4d_password 

```



# Current know issues:

1 - The custom template doesn't work. 
Error: “dictionary update sequence element #0 has length 16; 2 is required”


2 - if quality_report.absolute_geolocation_rms() does not exists, a simple try except won't be able to handle it. The program will crash.




