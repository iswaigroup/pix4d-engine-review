
FROM industrialskyworks/dpp-p4d-base:v4

MAINTAINER Ramin Fahimi <ramin.fahimi@Industrialskyworks.com>

ENTRYPOINT ["/bin/bash", "-c"]


RUN wget https://prod-pix4d-lib-public.s3.amazonaws.com/engine/1.3.0/python3-pix4dengine_1.3.0_amd64.deb
RUN apt-get install ./python3-pix4dengine_1.3.0_amd64.deb -y --fix-broken
RUN apt-get -o Dpkg::Options::='--force-confmiss' install --reinstall -y netbase

# Copy the application
COPY . /app

WORKDIR app/
